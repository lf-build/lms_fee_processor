﻿using LendFoundry.Foundation.Client;
using LMS.LoanAccounting;
using Newtonsoft.Json;

namespace LMS.FeeProcessor.Abstractions
{
    public interface IAccrualDetails
    {
        [JsonConverter(typeof(InterfaceConverter<IAccrualProcessRequest, AccrualProcessRequest>))]
        IAccrualProcessRequest AccrualProcessRequest { get; set; }
        bool IsStart { get; set; }
    }
}