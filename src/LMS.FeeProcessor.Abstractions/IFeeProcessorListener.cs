﻿namespace LMS.FeeProcessor.Abstractions
{
    public interface IFeeProcessorListener
    {
        void Start();
    }
}
