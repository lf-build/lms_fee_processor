﻿using LendFoundry.Security.Identity;
using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LMS.FeeProcessor
{
    public interface IFeeProcessorConfiguration : IDependencyConfiguration
    {
        EventMapping[] Events { get; set; }
        CaseInsensitiveDictionary<string> Mappings { get; set; }
        CaseInsensitiveDictionary<string> Statuses { get; set; }
        
        string ConnectionString { get; set; }
    }
}
