﻿namespace LMS.FeeProcessor.Abstractions
{
    public interface IAutoHoldRequest
    {
        bool IsAutoHold { get; set; }
    }
}