﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LMS.FeeProcessor.Abstractions
{
    public interface IFeeProcessorServiceFactory
    {
        IFeeProcessorService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
