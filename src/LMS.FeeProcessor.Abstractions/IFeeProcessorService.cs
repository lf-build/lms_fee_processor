﻿using LMS.LoanManagement.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.FeeProcessor.Abstractions
{
    public interface IFeeProcessorService
    {
        Task<List<IFeeDetails>> BuildFeeSchedule(string loanNumber);
        Task ProcessEvent(string loanNumber, string eventName, object data);
    }
}
