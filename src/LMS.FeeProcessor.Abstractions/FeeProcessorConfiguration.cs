﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Identity;

namespace LMS.FeeProcessor {
    public class FeeProcessorConfiguration : IFeeProcessorConfiguration, IDependencyConfiguration {
        public EventMapping[] Events { get; set; }
        public CaseInsensitiveDictionary<string> Mappings { get; set; }
        public CaseInsensitiveDictionary<string> Statuses { get; set; }
        public int DrawDownPercentage { get; set; }
        public string ReasonForHolding { get; set; }
        
        public Dictionary<string, string> Dependencies { get; set; }

        public string Database { get; set; }

        public string ConnectionString { get; set; }
    }
}