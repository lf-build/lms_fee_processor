﻿namespace LMS.FeeProcessor.Abstractions
{
    public class AutoHoldRequest : IAutoHoldRequest
    {
        public bool IsAutoHold { get; set; }
    }
}
