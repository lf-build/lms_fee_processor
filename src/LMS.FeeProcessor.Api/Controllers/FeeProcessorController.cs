﻿using LMS.FeeProcessor.Abstractions;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;
using System;
using LMS.LoanManagement.Abstractions;

namespace LMS.FeeProcessor.Api.Controllers
{
    /// <summary>
    /// FeeProcessorController
    /// </summary>
    [Route("/")]
    public class FeeProcessorController : ExtendedController
    {
        /// <summary>
        /// FeeProcessorController constructor
        /// </summary>
        /// <param name="feeProcessorService"></param>
        public FeeProcessorController(IFeeProcessorService feeProcessorService)
        {
            FeeProcessorService = feeProcessorService;
        }
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        private IFeeProcessorService FeeProcessorService { get; }

        /// <summary>
        /// BuildFeeSchedule
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <returns></returns>
        [HttpGet("/{loanNumber}/buildfeeschedule")]      
        [ProducesResponseType(typeof(IFeeDetails[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> BuildFeeSchedule(string loanNumber)
        {
            try
            {
                return Ok(await FeeProcessorService.BuildFeeSchedule(loanNumber));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

    }
}
