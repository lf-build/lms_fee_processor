﻿using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Foundation.Persistence.Mongo;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using LMS.FeeProcessor.Abstractions;
using LendFoundry.Foundation.Lookup.Client;
using System;
using LendFoundry.DataAttributes.Client;
using LMS.LoanManagement.Client;
using LMS.LoanAccounting.Client;
using LendFoundry.ProductRule.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.StatusManagement.Client;
using LendFoundry.EventHub.Client;
using LMS.Loan.Filters.Client;

namespace LMS.FeeProcessor.Api
{
   internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DataAttributes"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LMS.FeeProcessor.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            
            //services.AddMongoConfiguration(Settings.ServiceName);

#else
            services.AddSwaggerDocumentation();
#endif
            // services
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantService();
            services.AddEventHub( Settings.ServiceName);
            services.AddLookupService();
            services.AddDataAttributes();
            services.AddLoanManagementService();
            services.AddAccrualBalance();
            services.AddProductRuleService();
            services.AddDecisionEngine();
            services.AddStatusManagementService();
            services.AddLoanFilterService();
            services.AddConfigurationService<FeeProcessorConfiguration>(Settings.ServiceName);           
            services.AddDependencyServiceUriResolver<FeeProcessorConfiguration>(Settings.ServiceName);
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<FeeProcessorConfiguration>>().Get());

            // services.AddConfigurationService<FeeProcessorConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            //services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            // interface resolvers
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<FeeProcessorConfiguration>>().Get());

            services.AddTransient<IFeeProcessorService, FeeProcessorService>();
            services.AddTransient<IFeeProcessorServiceFactory, FeeProcessorServiceFactory>();

            services.AddTransient<IFeeProcessorConfiguration, FeeProcessorConfiguration>();

            services.AddTransient<IFeeProcessorListener, FeeProcessorListener>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
           
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Fee Processor Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseFeeProcessorListener();
            app.UseMvc();
          
        }
    }
}
