﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace LMS.FeeProcessor.Client
{
    public static class FeeProcessorServiceExtensions
    {
        public static IServiceCollection AddFeeProcessor(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IFeeProcessorServiceFactory>(p => new FeeProcessorServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IFeeProcessorServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddFeeProcessor(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IFeeProcessorServiceFactory>(p => new FeeProcessorServiceFactory(p, uri));
            services.AddSingleton(p => p.GetService<IFeeProcessorServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddFeeProcessor(this IServiceCollection services)
        {
            services.AddSingleton<IFeeProcessorServiceFactory>(p => new FeeProcessorServiceFactory(p));
            services.AddSingleton(p => p.GetService<IFeeProcessorServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

    }
}
