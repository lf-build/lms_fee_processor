﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Tenant.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using System.Linq;
using LMS.FeeProcessor.Abstractions;
using LendFoundry.Foundation.Services;


namespace LMS.FeeProcessor
{
    public class FeeProcessorListener : IFeeProcessorListener
    {
        #region Constructor

        public FeeProcessorListener(IConfigurationServiceFactory<FeeProcessorConfiguration> apiConfigurationFactory,
                                        IConfigurationServiceFactory configurationFactory,
                                        ITokenHandler tokenHandler,
                                        ILoggerFactory loggerFactory,
                                        IEventHubClientFactory eventHubFactory,
                                        ITenantServiceFactory tenantServiceFactory,
                                        ITenantTimeFactory tenantTimeFactory,
                                        IFeeProcessorServiceFactory feeProcessorServiceFactory)
        {
            ApiConfigurationFactory = apiConfigurationFactory;
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantTimeFactory = tenantTimeFactory;
            TenantServiceFactory = tenantServiceFactory;
            EventHubFactory = eventHubFactory;
            FeeProcessorServiceFactory = feeProcessorServiceFactory;
        }

        #endregion

        #region Variables

        private IConfigurationServiceFactory<FeeProcessorConfiguration> ApiConfigurationFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IFeeProcessorServiceFactory FeeProcessorServiceFactory { get; }

        #endregion

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info($"Eventhub listener started");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    try
                    {
                        logger.Info($"Initializing event hub for tenant #{tenant.Id}");
                        // Tenant token creation         
                        var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName, null, "system", null);
                        var reader = new StaticTokenReader(token.Value);

                        // Needed resources for this operation
                        var configuration = ApiConfigurationFactory.Create(reader).Get();
                        if (configuration == null)
                        {
                            logger.Warn($"Configuration not found for tenant #{tenant.Id}");
                            return;
                        }
                        if (configuration.Events == null)
                        {
                            logger.Info($"No events for tenant #{tenant.Id}");
                            return;
                        }                
                        var hub = EventHubFactory.Create(reader);
                        var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                        var feeProcessorService = FeeProcessorServiceFactory.Create(reader, TokenHandler, logger);
                        configuration
                            .Events
                            .ToList()
                            .ForEach(eventConfig =>
                                {
                                    hub.On(eventConfig.Name, AddView(eventConfig, logger, tenantTime, hub, feeProcessorService, configuration));
                                    logger.Info($"Event #{eventConfig.Name} attached to listener");
                                });
                        hub.StartAsync();
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Error while listening eventhub", ex);
                    }
                });
            }
            catch (Exception ex)
            {
                logger.Error("Error while listening eventhub", ex);
            }
        }

        private static Action<EventInfo> AddView
        (
            EventMapping eventConfiguration,
            ILogger logger,
            ITenantTime tenantTime,
            IEventHubClient eventHubService,
            IFeeProcessorService feeProcessorService,
            IFeeProcessorConfiguration configuration
        )
        {
            return @event =>
            {
                try
                {
                    var loanNumber = eventConfiguration.LoanNumber.FormatWith(@event);
                    logger.Info($"Processing for {loanNumber} with event {@event.Name}");
                    feeProcessorService.ProcessEvent(loanNumber, @event.Name, @event.Data).Wait();
                    logger.Info($"Processed event {@event.Name} for loan number : {loanNumber}");
                }
                catch (Exception ex)
                {
                    logger.Error($"Unhandled exception while listening event {@event.Name}", ex);
                }
            };
        }
    }
}
