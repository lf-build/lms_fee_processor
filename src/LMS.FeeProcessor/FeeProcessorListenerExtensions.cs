﻿using LMS.FeeProcessor.Abstractions;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif


namespace LMS.FeeProcessor
{
    public static class FeeProcessorListenerExtensions
    {
        public static void UseFeeProcessorListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IFeeProcessorListener>().Start();
        }
    }
}
