﻿using LMS.FeeProcessor.Abstractions;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.DataAttributes;
using System.Threading.Tasks;
using System.Collections.Generic;
using LMS.LoanManagement.Abstractions;
using Newtonsoft.Json;
using LMS.LoanManagement.Client;
using LMS.LoanAccounting;
using System.Linq;
using System;
using LendFoundry.ProductConfiguration;
using LendFoundry.ProductRule;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.StatusManagement;
using LMS.Loan.Filters.Abstractions;

namespace LMS.FeeProcessor
{
    public class FeeProcessorService : IFeeProcessorService
    {
        #region Constructor

        public FeeProcessorService(IEventHubClient eventHubClient,
                                    ITenantTime tenantTime,
                                    ILookupService lookup,
                                    IDataAttributesEngine dataAttributesEngine,
                                    ILoanManagementClientService loanManagementService,
                                    FeeProcessorConfiguration feeProcessorConfiguration,
                                    IAccrualBalanceService accrualBalanceService,
                                    IProductRuleService productRuleService,
                                    IDecisionEngineService decisionEngineService,
                                    IEntityStatusService statusManagementService,
                                    ILoanFilterService loanFilterService
                                    )
        {
            EventHub = eventHubClient;
            TenantTime = tenantTime;
            Lookup = lookup;
            LoanManagementService = loanManagementService;
            FeeProcessorConfiguration = feeProcessorConfiguration;
            AccrualBalanceService = accrualBalanceService;
            ProductRuleService = productRuleService;
            DecisionEngineService = decisionEngineService;
            DataAttributesEngine = dataAttributesEngine;
            StatusManagementService = statusManagementService;
            LoanFilterService = loanFilterService;
        }

        #endregion

        #region Private Variables

        private IEventHubClient EventHub { get; }
        private ITenantTime TenantTime { get; }
        private ILookupService Lookup { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private ILoanManagementClientService LoanManagementService { get; }
        private FeeProcessorConfiguration FeeProcessorConfiguration { get; }
        private IAccrualBalanceService AccrualBalanceService { get; }
        private IProductRuleService ProductRuleService { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private IEntityStatusService StatusManagementService { get; }
        private ILoanFilterService LoanFilterService { get; }

        #endregion

        public async Task RecordFee(string loanNumber, IFeeDetails feeDetails)
        {
            await LoanManagementService.AddFeeDetails(loanNumber, feeDetails);
        }

        public async Task ApplyFee(string loanNumber, IApplyFeeRequest applyFeeRequest)
        {
            await AccrualBalanceService.ApplyFee(loanNumber, applyFeeRequest);
        }

        public async Task StopAccrual(string loanNumber, IAccrualProcessRequest accrualProcessRequest)
        {
            await AccrualBalanceService.StopAccrual(loanNumber, "v1", accrualProcessRequest);
        }

        public async Task StartAccrual(string loanNumber, IAccrualProcessRequest accrualProcessRequest)
        {
            await AccrualBalanceService.StartAccrual(loanNumber, "v1", accrualProcessRequest);
        }

        public async Task StartStopAutoPay(string loanNumber, IAutoPayRequest autoPayRequest)
        {
            await LoanManagementService.UpdateAutoPayDetail(loanNumber, autoPayRequest);
        }

        public double ComputeFeeAmount(string ruleName, object payload)
        {
            var executionResult = DecisionEngineService.Execute<dynamic, object>(ruleName, new { payload = payload });
            return Convert.ToDouble(executionResult);
        }

        public bool ComputeRule(string ruleName, object payload)
        {
            var executionResult = DecisionEngineService.Execute<dynamic, bool>(ruleName, new { payload = payload });
            return executionResult;
        }

        public async Task<List<IProductFeeParameters>> GetProductFeeParametersBasedOnEvent(string productId, string loanNumber, string eventName)
        {
            var product = await GetProductParameters(productId, loanNumber);
            if (product == null)
                throw new NotFoundException($"Product details not found for product id : {productId} and loan number : {loanNumber}");

            var feeParameters = product.ProductFeeParameters.Where(f => f.ApplicableEvents != null && f.ApplicableEvents.Any(d => string.Equals(d, eventName, StringComparison.InvariantCultureIgnoreCase)));
            return feeParameters != null ? feeParameters.ToList<IProductFeeParameters>() : null;
        }

        public async Task<List<IProductParameters>> GetProductParametersBasedOnEvent(string productId, string loanNumber, string eventName)
        {
            var product = await GetProductParameters(productId, loanNumber);
            if (product == null)
                throw new NotFoundException($"Product details not found for product id : {productId} and loan number : {loanNumber}");

            var productParameters = product.ProductParameters.Where(c => c.ApplicableEvents != null && c.ApplicableEvents.Any(e => string.Equals(e, eventName, StringComparison.InvariantCultureIgnoreCase)));
            return productParameters.ToList<IProductParameters>();
        }

        public async Task<Tuple<List<IApplyFeeRequest>, List<IFeeDetails>>> EvaluateFee(string productId, string loanNumber, string eventName, ILoanInformation loanInformation, ILoanSchedule loanSchedule, IAccrual accrual, object data)
        {
            var parameters = await GetProductFeeParametersBasedOnEvent(productId, loanNumber, eventName);
            if (parameters == null || !parameters.Any())
                return null;

            var fees = new List<IApplyFeeRequest>();
            var feeDetails = new List<IFeeDetails>();
            foreach (var feeParameter in parameters)
            {
                double feeAmount = 0;
                if (!string.IsNullOrWhiteSpace(feeParameter.CalculationRule))
                {
                    var ruleDefinition = await ProductRuleService.GetRuleDefinitionById(feeParameter.CalculationRule);
                    if (ruleDefinition == null)
                        continue;

                    var input = new { EventName = eventName, LoanInformation = loanInformation, LoanSchedule = loanSchedule, Accrual = accrual, EventData = data, FeeProcessorConfiguration = FeeProcessorConfiguration };
                    feeAmount = ComputeFeeAmount(ruleDefinition.RuleDetail.DERuleName, input);
                }
                else
                {
                    feeAmount = feeParameter.FeeAmount;
                }

                var fee = new ApplyFeeRequest();
                fee.FeeName = feeParameter.FeeId;
                fee.ApplyDate = TenantTime.Now;
                fee.FeeType = feeParameter.FeeType;
                fee.Description = feeParameter.Description;
                fee.FeeAmount = feeAmount;
                fees.Add(fee);

                //if (fee.FeeType.ToLower() != "scheduled")
                //{
                //    var feeDetail = new FeeDetails();
                //    feeDetail.FeeName = feeParameter.Name;
                //    feeDetail.AutoSchedulePayment = true;
                //    feeDetail.DateInitiated = TenantTime.Now;
                //    feeDetail.Events = feeParameter.ApplicableEvents;
                //    feeDetail.FeeAmount = feeAmount;
                //    feeDetail.FeeDueDate = TenantTime.Now;
                //    feeDetail.FeeId = feeParameter.FeeId;
                //    feeDetail.FeeType = feeParameter.FeeType;
                //    feeDetail.IsProcessed = false;
                //    feeDetails.Add(feeDetail);
                //}
            }
            return Tuple.Create(fees, feeDetails);
        }

        public async Task<Tuple<List<IAccrualDetails>, List<IAutoPayRequest>, List<IAutoHoldRequest>>> EvaluateParameter(string productId, string loanNumber, string eventName, object data)
        {
            var parameters = await GetProductParametersBasedOnEvent(productId, loanNumber, eventName);
            if (parameters == null || !parameters.Any())
                return null;

            var accrualProcessRequests = new List<IAccrualDetails>();
            var autoPayRequests = new List<IAutoPayRequest>();
            var autoHoldRequests = new List<IAutoHoldRequest>();
            foreach (var parameter in parameters)
            {
                if (FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "stopaccrual" || FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "startaccrual")
                {
                    bool triggerAccrual = false;
                    if (!string.IsNullOrWhiteSpace(parameter.CalculationRuleId))
                    {
                        var ruleDefinition = await ProductRuleService.GetRuleDefinitionById(parameter.CalculationRuleId);
                        if (ruleDefinition == null)
                            continue;

                        var metrics = await LoanFilterService.GetMetrics(loanNumber);
                        var input = new { EventName = eventName, EventData = data, Metrics = metrics };
                        triggerAccrual = ComputeRule(ruleDefinition.RuleDetail.DERuleName, input);
                    }
                    else
                    {
                        triggerAccrual = parameter.Value == "true";
                    }
                    if (triggerAccrual)
                    {
                        var accrualDetails = new AccrualDetails();
                        accrualDetails.AccrualProcessRequest = new AccrualProcessRequest();
                        accrualDetails.AccrualProcessRequest.ProcessingDate = TenantTime.Now.DateTime;
                        accrualDetails.AccrualProcessRequest.IsAccrual = FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "startaccrual";
                        accrualDetails.IsStart = FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "startaccrual";
                        accrualProcessRequests.Add(accrualDetails);
                    }
                }
                else if (FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "stopautopay" || FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "startautopay")
                {
                    bool triggerAutoPay = false;
                    if (!string.IsNullOrWhiteSpace(parameter.CalculationRuleId))
                    {
                        var ruleDefinition = await ProductRuleService.GetRuleDefinitionById(parameter.CalculationRuleId);
                        if (ruleDefinition == null)
                            continue;

                        var metrics = await LoanFilterService.GetMetrics(loanNumber);
                        var input = new { EventName = eventName, EventData = data, Metrics = metrics };
                        triggerAutoPay = ComputeRule(ruleDefinition.RuleDetail.DERuleName, input);
                    }
                    else
                    {
                        triggerAutoPay = parameter.Value == "true";
                    }
                    if (triggerAutoPay)
                    {
                        var autoPayRequest = new AutoPayRequest();
                        autoPayRequest.IsAutopay = FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "startautopay";
                        autoPayRequest.AutoPayStartDate = FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "startautopay" ? TenantTime.Now : (DateTimeOffset?)null;
                        autoPayRequest.AutoPayStopDate = FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "startautopay" ? (DateTimeOffset?)null : TenantTime.Now;
                        autoPayRequests.Add(autoPayRequest);
                    }
                }
                else if (FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "autoholdlineon" || FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "autounholdlineon")
                {
                    bool triggerAutoHold = false;
                    if (!string.IsNullOrWhiteSpace(parameter.CalculationRuleId))
                    {
                        var ruleDefinition = await ProductRuleService.GetRuleDefinitionById(parameter.CalculationRuleId);
                        if (ruleDefinition == null)
                            continue;

                        var metrics = await LoanFilterService.GetMetrics(loanNumber);
                        var input = new { EventName = eventName, EventData = data, Metrics = metrics };
                        triggerAutoHold = ComputeRule(ruleDefinition.RuleDetail.DERuleName, input);
                    }
                    else
                    {
                        triggerAutoHold = parameter.Value == "true";
                    }
                    if (triggerAutoHold)
                    {
                        var autoHoldRequest = new AutoHoldRequest();
                        autoHoldRequest.IsAutoHold = FeeProcessorConfiguration.Mappings[parameter.ParameterId] == "autoholdlineon";
                        autoHoldRequests.Add(autoHoldRequest);
                    }
                }
            }
            return Tuple.Create(accrualProcessRequests, autoPayRequests, autoHoldRequests);
        }

        public async Task ProcessEvent(string loanNumber, string eventName, object data)
        {
            var loanInformation = await LoanManagementService.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
                throw new NotFoundException($"Loan details not found for loan number: {loanNumber}");

            var loanSchedule = await LoanManagementService.GetLoanSchedule(loanNumber);
            if (loanSchedule == null)
                throw new NotFoundException($"Active loan schedule details not found for loan number: {loanNumber}");

            var loanAccounting = await AccrualBalanceService.GetLoanByScheduleVersion(loanNumber, loanSchedule.ScheduleVersionNo);
            if (loanAccounting == null)
                throw new NotFoundException($"Loan accounting details not found for loan number: {loanNumber} with schedule version: {loanSchedule.ScheduleVersionNo}");

            var fees = await EvaluateFee(loanInformation.LoanProductId, loanNumber, eventName, loanInformation, loanSchedule, loanAccounting, data);
            if (fees != null && fees.Item1.Any())
            {
                if (fees.Item1 != null && fees.Item1.Any())
                {
                    foreach (var fee in fees.Item1)
                    {
                        await ApplyFee(loanNumber, fee);

                    }
                }
                //if (fees.Item2 != null && fees.Item2.Any())
                //{
                //    foreach (var feeDetail in fees.Item2)
                //    {
                //        await RecordFee(loanNumber, feeDetail);
                //    }
                //}
            }

            var parameters = await EvaluateParameter(loanInformation.LoanProductId, loanNumber, eventName, data);
            if (parameters != null)
            {
                if (parameters.Item1 != null && parameters.Item1.Any())
                {

                    foreach (var accrualRequest in parameters.Item1)
                    {
                        if (accrualRequest.IsStart)
                        {
                            if (loanAccounting.AccrualStop)
                            {
                                await StartAccrual(loanNumber, accrualRequest.AccrualProcessRequest);
                            }
                        }
                        else
                        {
                            if (!loanAccounting.AccrualStop)
                            {
                                await StopAccrual(loanNumber, accrualRequest.AccrualProcessRequest);
                            }

                        }
                    }
                }
                if (parameters.Item2 != null && parameters.Item2.Any())
                {
                    foreach (var autoPayRequest in parameters.Item2)
                    {                       
                        var lastHistory = loanInformation.AutoPayHistory != null &&  loanInformation.AutoPayHistory.Count > 0 ?  loanInformation.AutoPayHistory.LastOrDefault():null;
                         var autoPayFlag = lastHistory != null  ? lastHistory.IsAutoPay : loanInformation.IsAutoPay;
                        
                        if (autoPayFlag != autoPayRequest.IsAutopay)
                        {
                            if(lastHistory != null )
                            {
                                if(lastHistory.ResumeDate != null && lastHistory.StopDate != null &&( TenantTime.Now.Date >= lastHistory.StopDate.Time.Date &&   TenantTime.Now.Date <=lastHistory.ResumeDate.Time.Date) )
                                {
                                    await StartStopAutoPay(loanNumber, autoPayRequest);
                                }
                                else if(lastHistory.ResumeDate.Time.Date == null && TenantTime.Now.Date >= lastHistory.StopDate.Time.Date )
                                {
                                    await StartStopAutoPay(loanNumber, autoPayRequest);
                                }
                                else if(lastHistory.StopDate == null )
                                {
                                    await StartStopAutoPay(loanNumber, autoPayRequest);
                                }
                            }
                            else{
                                await StartStopAutoPay(loanNumber, autoPayRequest);
                            }                            
                        }
                    }
                }
                if (parameters.Item3 != null && parameters.Item3.Any())
                {
                    var statusFlow = await StatusManagementService.GetActiveStatusWorkFlow("application", loanInformation.LoanApplicationNumber);
                    foreach (var autoHoldRequest in parameters.Item3)
                    {
                        if (autoHoldRequest.IsAutoHold)
                        {
                            var requestModel = new RequestModel { reasons = new List<string> { FeeProcessorConfiguration.ReasonForHolding } };
                            await StatusManagementService.ChangeStatus("application", loanInformation.LoanApplicationNumber, statusFlow != null ? statusFlow.StatusWorkFlowId : null, FeeProcessorConfiguration.Statuses["Hold"], requestModel);
                        }
                        else
                        {
                            await StatusManagementService.ChangeStatus("application", loanInformation.LoanApplicationNumber, statusFlow != null ? statusFlow.StatusWorkFlowId : null, FeeProcessorConfiguration.Statuses["UnHold"], new RequestModel());
                        }
                    }
                }
            }
        }

        public async Task<ProductDetails> GetProductParameters(string productId, string loanNumber)
        {
            var productDetails = await DataAttributesEngine.GetAttribute("loan", loanNumber, "product");
            var productObject = JsonConvert.DeserializeObject<List<ProductDetails>>(productDetails.ToString());
            return productObject.FirstOrDefault();
        }

        public async Task<List<IFeeDetails>> BuildFeeSchedule(string loanNumber)
        {
            var loanInformation = await LoanManagementService.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
                throw new NotFoundException($"Loan details not found for loan number: {loanNumber}");

            var product = await GetProductParameters(loanInformation.LoanProductId, loanNumber);
            if (product == null)
                throw new NotFoundException($"Product details not found for product id : {loanInformation.LoanProductId} and loan number : {loanNumber}");

            var feeParameters = product.ProductFeeParameters.Where(f => string.Equals(f.FeeType.ToLower(), "scheduled", StringComparison.InvariantCultureIgnoreCase));
            if (feeParameters == null)
                return null;

            var loanSchedule = await LoanManagementService.GetLoanSchedule(loanNumber);
            if (loanSchedule == null)
                throw new NotFoundException($"Active loan schedule details not found for loan number: {loanNumber}");

            var feeDetails = new List<IFeeDetails>();
            foreach (var feeParameter in feeParameters)
            {
                var ruleDefinition = await ProductRuleService.GetRuleDefinitionById(feeParameter.CalculationRule);
                if (ruleDefinition == null)
                    throw new NotFoundException($"Rule {feeParameter.CalculationRule} not found ");

                var payload = new { LoanSchedule = loanSchedule };
                var executionResult = DecisionEngineService.Execute<dynamic, List<FeeDetails>>(ruleDefinition.RuleDetail.DERuleName, new { payload });
                feeDetails.AddRange(executionResult);
            }
            return feeDetails;
        }

    }
}