﻿using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.EventHub;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.DataAttributes.Client;
using LMS.FeeProcessor.Abstractions;
using LMS.LoanManagement.Client;
using LMS.LoanAccounting;
using LendFoundry.ProductRule.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.StatusManagement.Client;
using LMS.Loan.Filters.Client;

namespace LMS.FeeProcessor
{
    public class FeeProcessorServiceFactory : IFeeProcessorServiceFactory
    {
        public FeeProcessorServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public IFeeProcessorService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var feeProcessorConfigurationService = configurationServiceFactory.Create<FeeProcessorConfiguration>(Settings.ServiceName, reader);
            var feeProcessorConfiguration = feeProcessorConfigurationService.Get();

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var lookupServiceFactory = Provider.GetService<ILookupClientFactory>();
            var lookupService = lookupServiceFactory.Create(reader);

            var dataAttributesServiceFactory = Provider.GetService<IDataAttributesClientFactory>();
            var dataAttributesService = dataAttributesServiceFactory.Create(reader);

            var loanManagementServiceFactory = Provider.GetService<ILoanManagementClientServiceFactory>();
            var loanManagementService = loanManagementServiceFactory.Create(reader);

            var accrualServiceFactory = Provider.GetService<IAccrualBalanceClientFactory>();
            var accrualService = accrualServiceFactory.Create(reader);

            var productRuleServiceFactory = Provider.GetService<IProductRuleServiceClientFactory>();
            var productRuleService = productRuleServiceFactory.Create(reader);

            var decisionEngineServiceFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngineService = decisionEngineServiceFactory.Create(reader);

            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory>();
            var statusManagementService = statusManagementServiceFactory.Create(reader);

            var loanFilterServiceFactory = Provider.GetService<ILoanFilterClientServiceFactory>();
            var loanFilterService = loanFilterServiceFactory.Create(reader);

            return new FeeProcessorService(eventHub, tenantTime, lookupService, dataAttributesService, loanManagementService, feeProcessorConfiguration, accrualService, productRuleService, decisionEngineService, statusManagementService, loanFilterService);
        }
    }
}
